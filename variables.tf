variable "gcp_project" {
  description = "GPC project the cluster will be deployed"
}
variable "cluster_location" {
  default = "us-central1-a"
}
variable "gcp_sa_project" {
  default = "techtur-bid-commons"
}
variable "service-account_key" {
  description = "GCP service-account JSON key"
}

variable "cluster_env" {
  description = "Defines the environment of this deployment"
}

variable "cluster_name" {
  description = "Cluster name"
}

variable "cluster_version" {
  default     = "latest"
  description = "Set an specific gke version or leave blank to use the latest"
}

variable "cluster_region" {
  default     = "us-central1"
  description = "Cluster region"
}

variable "cluster_zone" {
  default     = "us-central1-a"
  description = "Cluster zone"
}

variable "preemptible_nodes" {
  default     = "true"
  description = "(true/false) Wether to use preemptible nodes or not in this workspace"
}

variable "node_count" {
  default     = "3"
  description = "Number of nodes to deployed on each zone"
}

variable "machine_type" {
  default     = "n1-standard-2"
  description = "GCE machine type to be used by the nodes"
}

variable "nodes_disk_size" {
  default = "50"
  description = "Disk size for each node"
}

variable "cloud-dns_project" {
  default = "techtur-bid-commons"
  description = "GCP project used by Cloud DNS to manage the zone "
}

variable "cloud-dns_zone-name" {
  default     = "techtur-bid"
  description = "GCP project used by Cloud DNS to manage the zone "
}

variable "daily_maintenance_window_start_time" {
  default = "01:00"
  description = <<EOF
The start time of the 4 hour window for daily maintenance operations RFC3339
format HH:MM, where HH : [00-23] and MM : [00-59] GMT.
EOF
}

variable "http_load_balancing_disabled" {
  default = "false"
  description = <<EOF
The status of the HTTP (L7) load balancing controller addon, which makes it
easy to set up HTTP load balancers for services in a cluster. It is enabled
by default; set disabled = true to disable.
EOF
}