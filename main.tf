module "gke-vpc" {
  source       = "app.terraform.io/techturbid/gke_vpc/google"
  version      = ">= 0.2.1"
  CLUSTER_NAME = var.cluster_name
  wait_for_resources = ["1"]
}

module "gke" {
  source  = "app.terraform.io/techturbid/gke/google"
  version = "0.3.1"

  cluster_secondary_range_name        = module.gke-vpc.cluster_secondary_range_name
  services_secondary_range_name       = module.gke-vpc.services_secondary_range_name
  vpc_network_name                    = module.gke-vpc.vpc_network_name
  vpc_subnetwork_name                 = module.gke-vpc.vpc_subnetwork_name
  http_load_balancing_disabled        = var.http_load_balancing_disabled
  CLUSTER_LOCATION = var.cluster_location
  CLUSTER_NAME = var.cluster_name
  CLUSTER_VERSION = var.cluster_version
  GCP_PROJECT = var.gcp_project
  GCP_SA_PROJECT = var.gcp_sa_project
  MACHINE_TYPE = var.machine_type
  NODES_DISK_SIZE = var.nodes_disk_size
  NODE_COUNT = var.node_count
  PREEMPTIBLE_NODES = var.preemptible_nodes
  REGIONAL_CLUSTER = "false"
  wait_for_resources = [module.gke-vpc.vpc_subnetwork_name]
}