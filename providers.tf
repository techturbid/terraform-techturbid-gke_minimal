provider "google" {
  credentials = var.service-account_key
  project     = var.gcp_project
  region      = var.cluster_region
}

provider "google-beta" {
  credentials = var.service-account_key
  project = var.gcp_project
  region  = var.cluster_region

}

data "google_client_config" "google_current" {
  provider = "google"
}

data "google_client_config" "google-beta_current" {
  provider = "google-beta"
}